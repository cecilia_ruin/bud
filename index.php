<?php
require 'config/config.php';
// custom autoloader
require 'helpers/Autoload.php';
// composer autoloader
require 'vendor/autoload.php';

Autoload::register();

// Création d'une nouvelle instance de Book

$page = $_GET['page'] ?? 'home';

if ($page === 'home') {
    $controller = new App\Controllers\Cagnote();
    $controller->index();
} else {
    // book/show
    $page = explode('/', $page);

    $controller_name = 'App\\Controllers\\' . ucfirst($page[0]);
    $method_name = $page[1];

    $router = new $controller_name();

    $router->$method_name();
}
