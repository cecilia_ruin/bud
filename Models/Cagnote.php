<?php
namespace App\Models;
use Exception;
use PDO;
use PDOException;


class Cagnote extends Model{
    private $id_Cagnote;
    private $name_Cagnote;
    private $introduction_Cagnote;

    protected $table = 'cagnote';

    public function getAllMoneyComeByCagnote(){
      
        $stmt = $this->db->prepare('SELECT `id_Moneycome`,`name_Moneycome`,`money_Moneycome`
         FROM `moneycome` WHERE `id_Cagnote` = ?');

        // on exécute la requête en précisant la valeur du paramètre

        $stmt->execute([$this->id_Cagnote]);
        //  on extrait les données de la réponse réçue.
        $money = $stmt->fetchall(\PDO::FETCH_OBJ);
        return $money;
    } 

    public function getTotalMoneyComeByCagnote(){
      
        $stmt = $this->db->prepare('SELECT SUM(`money_MoneyCome`) as totalmoney
         FROM `moneycome` WHERE `id_Cagnote` = ?');

        // on exécute la requête en précisant la valeur du paramètre

        $stmt->execute([$this->id_Cagnote]);
        //  on extrait les données de la réponse réçue.
        $totalmoney = $stmt->fetch(\PDO::FETCH_OBJ);
        return $totalmoney;
    } 

    function deletecontent()
    {
        // on utilise la méthode prepare car notre requête a pas besoin d'un paramètre envoyé par l'utilisateur.

        $stmt = $this->db->prepare('DELETE FROM `moneycome` WHERE `id_Cagnote` = ?');

        // on exécute la requête en précisant la valeur du paramètre
        return $stmt->execute([$this->id_Cagnote]);
    }

    public function create()
    {
        $sql = "INSERT INTO `cagnote` (`name_Cagnote`, `introduction_Cagnote`) VALUES (:name_Cagnote, :introduction_Cagnote)";

        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(':name_Cagnote', $this->name_Cagnote, PDO::PARAM_STR);
        $stmt->bindValue(':introduction_Cagnote', $this->introduction_Cagnote, PDO::PARAM_STR);

        try {
            $result = $stmt->execute();
            return $result;
        } catch (PDOException $e) {
            echo sprintf('Erreur lors de l\'insertion des données : %s', $e->getMessage());

            throw new Exception('Echec de l\'insertion du livre');
        }
    }
    public function update()
    {
        $sql = "UPDATE `cagnote` SET `name_Cagnote` = :name_Cagnote , `introduction_Cagnote` = :introduction_Cagnote WHERE `id_Cagnote` = :id";
        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(':name_Cagnote', $this->name_Cagnote, PDO::PARAM_STR);
        $stmt->bindValue(':introduction_Cagnote', $this->introduction_Cagnote, PDO::PARAM_STR);
        $stmt->bindValue(':id', $this->id_Cagnote, PDO::PARAM_INT);

        try {
            $result = $stmt->execute();
            return $result;
        } catch (PDOException $e) {
            echo sprintf('Erreur lors de la modification des données : %s', $e->getMessage());
            return false;
        }
    }

    
    
    // Generic getter
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }


    /**
     * Get the value of id_Cagnote
     */ 
    public function getId_Cagnote()
    {
        return $this->id_Cagnote;
    }

    /**
     * Set the value of id_Cagnote
     *
     * @return  self
     */ 
    public function setId_Cagnote($id_Cagnote)
    {
        $this->id_Cagnote = $id_Cagnote;

        return $this;
    }

    /**
     * Get the value of name_Cagnote
     */ 
    public function getName_Cagnote()
    {
        return $this->name_Cagnote;
    }

    /**
     * Set the value of name_Cagnote
     *
     * @return  self
     */ 
    public function setName_Cagnote($name_Cagnote)
    {
        $this->name_Cagnote = $name_Cagnote;

        return $this;
    }

    /**
     * Get the value of introduction_Cagnote
     */ 
    public function getIntroduction_Cagnote()
    {
        return $this->introduction_Cagnote;
    }

    /**
     * Set the value of introduction_Cagnote
     *
     * @return  self
     */ 
    public function setIntroduction_Cagnote($introduction_Cagnote)
    {
        $this->introduction_Cagnote = $introduction_Cagnote;

        return $this;
    }
    }
