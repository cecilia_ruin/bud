<?php
namespace App\Models;
namespace App\Models;
use Exception;
use PDO;
use PDOException;

class Categorie extends Model{
    private $id;
    private $name;

    protected $table = 'categorie';

    public function create()
    {
        $sql = "INSERT INTO `categorie` (`name_Categorie`) VALUES (:name_Categorie)";

        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(':name_Categorie', $this->name, PDO::PARAM_STR);

        try {
            $result = $stmt->execute();
            return $result;
        } catch (PDOException $e) {
            echo sprintf('Erreur lors de l\'insertion des données : %s', $e->getMessage());

            throw new Exception('Echec de l\'insertion du livre');
        }
    }
    public function update()
    {
        $sql = "UPDATE `categorie` SET `name_Categorie` = :name_Categorie WHERE `id_Categorie` = :id";
        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(':name_Categorie', $this->name, PDO::PARAM_STR);
        $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);

        try {
            $result = $stmt->execute();
            return $result;
        } catch (PDOException $e) {
            echo sprintf('Erreur lors de la modification des données : %s', $e->getMessage());
            return false;
        }
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}
