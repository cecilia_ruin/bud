<?php
namespace App\Models;
namespace App\Models;
use Exception;
use PDO;
use PDOException;

class Clients extends Model{
    private $id;
    private $name;
    private $mail;
    private $pwd;
    private $id_cagnote;

    protected $table = 'clients';

    public function create()
    {
        $sql = "INSERT INTO `clients` (`name_clients`, `mail_clients`, `password_client`,`id_Cagnote`) VALUES (:name_clients, :mail_clients, :password_client, :id_Cagnote)";

        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(':name_clients', $this->name, PDO::PARAM_STR);
        $stmt->bindValue(':mail_clients', $this->mail, PDO::PARAM_STR);
        $stmt->bindValue(':password_client', $this->pwd, PDO::PARAM_STR);
        $stmt->bindValue(':name_clients', $this->id_cagnote, PDO::PARAM_STR);

        try {
            $result = $stmt->execute();
            return $result;
        } catch (PDOException $e) {
            echo sprintf('Erreur lors de l\'insertion des données : %s', $e->getMessage());

            throw new Exception('Echec de l\'insertion du livre');
        }
    }
    public function update()
    {
        $sql = "UPDATE `categorie` SET `name_Categorie` = :name_Categorie WHERE `id_Categorie` = :id";
        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(':name_Categorie', $this->name, PDO::PARAM_STR);
        $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);

        try {
            $result = $stmt->execute();
            return $result;
        } catch (PDOException $e) {
            echo sprintf('Erreur lors de la modification des données : %s', $e->getMessage());
            return false;
        }
    }
}