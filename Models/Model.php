<?php
namespace App\Models;
use App\Databases\Database;

abstract class Model
{
    protected $db;

    protected $table;

    /* Constructeur par défault*/
    public function __construct()
    {
        $this->db = Database::getInstance();
    }

    /*Fonction hydrate inserre des données dans un objet*/
    public function hydrate(array $data)
    {
        foreach ($data as $key => $val) {
            $method = 'set' . ucfirst($key);

            if (method_exists($this, $method)) {
                $this->$method($val);
            }
        }
    }
    
    
    function delete($condition='id')
    {
        // on utilise la méthode prepare car notre requête a pas besoin d'un paramètre envoyé par l'utilisateur.

        $stmt = $this->db->prepare('DELETE FROM '. $this->table .' WHERE '. $condition .' = ?');

        // on exécute la requête en précisant la valeur du paramètre
        $id_name='id_'.ucfirst($this->table);
        return $stmt->execute([$this->$id_name]);
    }

    public function getOne($condition='id')
    {
        // on utilise la méthode prepare car notre requête a pas besoin d'un paramètre envoyé par l'utilisateur.

        $stmt = $this->db->prepare('SELECT * FROM ' . $this->table . ' WHERE  '.$condition.' = ?');
        $id_name='id_'.ucfirst($this->table);
        // on exécute la requête en précisant la valeur du paramètre
        $stmt->execute([$this->$id_name]);

        //  on extrait les données de la réponse réçue.

        $cagnote = $stmt->fetch();
    

        $this->hydrate($cagnote);
    }

    public function getAll()
    {
        // on utilise la méthode prepare car notre requête a pas besoin d'un paramètre envoyé par l'utilisateur.

        $stmt = $this->db->query('SELECT * FROM ' . $this->table);

        // on exécute la requête en précisant la valeur du paramètre

        //  on extrait les données de la réponse réçue.

        $authors = $stmt->fetchAll();

        return $authors;
    }

    /**
     * Get the value of db
     */ 
    public function getDb()
    {
        return $this->db;
    }
}
