<?php
namespace App\Models;
use Exception;
use PDO;
use PDOException;

class MoneyCome extends Model{
    private $id_Moneycome;
    private $name_Moneycome;
    private $money_Moneycome;
    private $id_Cagnote;
    private $id_Categorie;
    private $id_Type;

    protected $table = 'moneycome';

    public function getAllBycategories(){
        $sql = "SELECT
            `money_MoneyCome`
            `id_Categorie`,
            `name_Categorie`,
         FROM
            `moneycome`, `categorie`
        INNER JOIN `categorie` ON `moneycome`.`id_Categorie` = `categorie`.`id_Categorie`";
        // on utilise la méthode query car notre requête n'a pas besoin de paramètre.
        $stmt = $this->db->query($sql);
        //  on extrait les données de la réponse réçue.
        $sum = $stmt->fetch();
        return $sum;
    }
    
    public function getsum(){
        $stmt = $this->db->prepare('SELECT SUM(`money_MoneyCome`) as totalmoney
        FROM `moneycome` WHERE `id_Cagnote` = ?');

       // on exécute la requête en précisant la valeur du paramètre

       $stmt->execute([$this->id_Cagnote]);
       //  on extrait les données de la réponse réçue.
       $totalmoney = $stmt->fetch(\PDO::FETCH_OBJ);
       return $totalmoney;
    }

    public function create()
    {
        $sql = "INSERT INTO `moneycome` (`name_Moneycome`, `money_Moneycome`, `id_Cagnote`, `id_Categorie`, `id_Type`) VALUES (:name_Moneycome, :money_Moneycome, :id_Cagnote, :id_Categorie, :id_Type)";

        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(':name_Moneycome', $this->name_Moneycome, PDO::PARAM_STR);
        $stmt->bindValue(':money_Moneycome', $this->money_Moneycome, PDO::PARAM_STR);
        $stmt->bindValue(':id_Cagnote', $this->id_Cagnote, PDO::PARAM_INT);
        $stmt->bindValue(':id_Categorie', $this->id_Categorie, PDO::PARAM_INT);
        $stmt->bindValue(':id_Type', $this->id_Type, PDO::PARAM_INT);


        try {
            $result = $stmt->execute();
            return $result;
        } catch (PDOException $e) {
            echo sprintf('Erreur lors de l\'insertion des données : %s', $e->getMessage());

            throw new Exception('Echec de l\'insertion de la somme');
        }
    }

    /**
     * GET / POST
     * Met à jour les données d'une somme d'argent
     * @return void
     */
    public function update()
    {
        $sql = "UPDATE `moneycome` SET `name_Moneycome` = :name_Moneycome , `money_Moneycome` = :money_Moneycome , `id_Cagnote`= :id_Cagnote , `id_Categorie`= :id_Categorie , `id_Type`=:id_Type  WHERE `id_Moneycome` = :id_Moneycome";
        $stmt = $this->db->prepare($sql);

        $stmt->bindValue(':name_Moneycome', $this->name_Moneycome, PDO::PARAM_STR);
        $stmt->bindValue(':money_Moneycome', $this->money_Moneycome, PDO::PARAM_STR);
        $stmt->bindValue(':id_Moneycome', $this->id_Moneycome, PDO::PARAM_INT);
        $stmt->bindValue(':id_Cagnote', $this->id_Cagnote, PDO::PARAM_INT);
        $stmt->bindValue(':id_Categorie', $this->id_Categorie, PDO::PARAM_INT);
        $stmt->bindValue(':id_Type', $this->id_Type, PDO::PARAM_INT);
        try {
            $result = $stmt->execute();
            return $result;
        } catch (PDOException $e) {
            echo sprintf('Erreur lors de la modification des données : %s', $e->getMessage());
            return false;
        }
    }
    


    // Generic getter
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }
    

    /**
     * Get the value of id_Cagnote
     */ 
    public function getId_Cagnote()
    {
        return $this->id_Cagnote;
    }

    /**
     * Set the value of id_Cagnote
     *
     * @return  self
     */ 
    public function setId_Cagnote($id_Cagnote)
    {
        $this->id_Cagnote = $id_Cagnote;

        return $this;
    }

    /**
     * Get the value of id_Category
     */ 
    public function getId_Categorie()
    {
        return $this->id_Categorie;
    }

    /**
     * Set the value of id_Category
     *
     * @return  self
     */ 
    public function setId_Categorie($id_Categorie)
    {
        $this->id_Categorie = $id_Categorie;

        return $this;
    }

    /**
     * Get the value of id_Type
     */ 
    public function getId_Type()
    {
        return $this->id_Type;
    }

    /**
     * Set the value of id_Type
     *
     * @return  self
     */ 
    public function setId_Type($id_Type)
    {
        $this->id_Type = $id_Type;

        return $this;
    }


    /**
     * Get the value of id_Moneycome
     */ 
    public function getId_Moneycome()
    {
        return $this->id_Moneycome;
    }

    /**
     * Set the value of id_Moneycome
     *
     * @return  self
     */ 
    public function setId_Moneycome($id_Moneycome)
    {
        $this->id_Moneycome = $id_Moneycome;

        return $this;
    }

    /**
     * Get the value of name_Moneycome
     */ 
    public function getName_Moneycome()
    {
        return $this->name_Moneycome;
    }

    /**
     * Set the value of name_Moneycome
     *
     * @return  self
     */ 
    public function setName_Moneycome($name_Moneycome)
    {
        $this->name_Moneycome = $name_Moneycome;

        return $this;
    }

    /**
     * Get the value of money_Moneycome
     */ 
    public function getMoney_Moneycome()
    {
        return $this->money_Moneycome;
    }

    /**
     * Set the value of money_Moneycome
     *
     * @return  self
     */ 
    public function setMoney_Moneycome($money_Moneycome)
    {
        $this->money_Moneycome = $money_Moneycome;

        return $this;
    }
}

    