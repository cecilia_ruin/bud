<?php
namespace App\Helpers;

class Validator
{
    public static function checkId($id)
    {
        // on vérifie l'existance d'un paramètre id, non null, de type numérique
        if (!empty($id) && ctype_digit($id)) {
            // on nettoie le paramètre pour se protéger contre les injections de code.
            return filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        }
        return null;
    }
}