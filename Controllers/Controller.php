<?php
namespace App\Controllers;

use App\Helpers\HttpRequest;

class Controller{
    protected $twig;
    public function __construct()
    {
        $loader = new \Twig\Loader\FilesystemLoader(ROOT . '/templates');
        $this->twig = new \Twig\Environment($loader, [
            'cache' => false, 
        ]);
    }

    public function __call($name, $arguments)
    {
        HttpRequest::HttpNotFound();

        $this->notFound();
    }

    public function notFound()
    {
        $title = 'Page d\'erreur 404';
        $heading = '';
        $this->render('errors/404', compact('title', 'heading'));
    }
}

