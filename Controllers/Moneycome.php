<?php

namespace App\Controllers;

use App\Helpers\Validator;
use App\Helpers\HttpRequest;
use App\Models\MoneyCome as MoneyModel;
use App\Models\Cagnote as CagnoteModel;
use App\Models\Type as TypeModel;
use App\Models\Categorie as CategorieModel;
use App\libraries\Form_Validator;

class Moneycome extends Controller
{
    /**
     * GET / POST
     * Affiche le formulaire de création 
     * Enregistre les données du formulaire dans la base de données
     * @return void
     */
    public function create()
    {
        // on initialise le tableau de données qui servira à pré-remplir les champs du formulaire
        $data = [
            'name_Moneycome' => '',
            'money_Moneycome' => '',
            'id_Cagnote' => '',
            'id_Type' => '',
            'id_Categorie' => '',
        ];
        // on initialise le tableau des erreurs à envoyer à la vue
        $errors = array();
        // On vérifie s'il y a une requête HTTP POST
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // nettoyage des données utilisateurs

            // validation des données

            $data = $_POST;
            // die;
            // on crée une instance de la librairie de validation
            $form_validator = new Form_Validator();
            
            // on valide les données du formulaire avec les règles définies dans la table passé en second argument.
            $form_validator->make($data, [
                'name_Moneycome' => 'required|alpha',
                'money_Moneycome' => 'required|float',
                'id_Cagnote' => 'required|int',
                'id_Type' => 'required|int',
                'id_Categorie' => 'required|int',
            ]);

            // si le formulaire est valide on continue le processus de création
            if ($form_validator->isValid()) {
                // insertion dans la base de données
                // création d'une instance de Moneycome

                $money = new MoneyModel();

                $money->hydrate($data); 
                  
                if (($money->id_Type == 2 ) && ($money->money_Moneycome < $money->getsum())) {
                    $money->setMoney_Moneycome(- $money->money_Moneycome);
                    
                
                    if ($money->create()) {
                        HttpRequest::redirectTo('index');
                    }
                }else if($money->id_Type == 1){
                    if ($money->create()) {
                        HttpRequest::redirectTo('index');
                    }
                }
                else{ echo "insertion impossible";}
            }
            // On renseigne le table d'erreurs passé à vue en cas d'erreurs.
            $errors = $form_validator->getErrors();
        }
        // On récupère la liste des types et catégories pour peupler les options du select
        $type = new TypeModel();
        $types = $type->getAll();
        $categorie = new CategorieModel();
        $categories = $categorie->getAll();
        $cagnote = new CagnoteModel();
        $cagnotes = $cagnote->getAll();
        $title = 'Ajouter une somme';
        $heading = 'Enregister une nouvelle somme';
        echo $this->twig->render('moneycomes/create.html.twig', compact('title', 'heading', 'cagnotes', 'types', 'categories', 'data', 'errors'));
    }


    /**
     * GET / POST
     * Affiche le formulaire de modification 
     * Mets à jour les données
     * @return void
     */
    public function update()
    {
        /**
         * 1. On récupère le paramètre d'url id
         */
        // on déclare la variable qui va récevoir l'id et on l'initialise avec rien.
        $money_id = Validator::checkId($_GET['id']);

        // s'il n'existe pas de paramètre, on affiche le message d'erreur

        if (!$money_id) {
            HttpRequest::redirectTo('index');
        }

        // Nouvel instance d'un somme 

        $money = new MoneyModel();

        $money->setId_Moneycome($money_id);

        // récuparation d'une cagnote

        $money->getOne('id_Moneycome');

        /**
         * On affiche la page
         */
        $title = $money->name_Moneycome;

        $heading = $money->name_Moneycome;


        // partie création de la somme

        // on initialise le tableau de données qui servira à pré-remplir les champs du formulaire
        $data = [
            "id_Moneycome" => $money->id_Moneycome,
            'name_Moneycome' => $money->name_Moneycome,
            'money_Moneycome' => $money->money_Moneycome,
            'id_Cagnote' => $money->id_Cagnote,
            'id_Categorie' => $money->id_Categorie,
            'id_Type' => $money->id_Type,

        ];
        // on initialise le tableau des erreurs à envoyer à la vue
        $errors = array();
        // On vérifie s'il y a une requête HTTP POST
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // nettoyage des données utilisateurs

            // validation des données

            $data = $_POST;
            // on crée une instance de la librairie de validation
            $form_validator = new Form_Validator();

            // on valide les données du formulaire avec les règles définies dans la table passé en second argument.
            $form_validator->make($data, [
                'name_Moneycome' => 'required|alpha',
                'money_Moneycome' => 'required|float',
                'id_Cagnote' => 'required|int',
                'id_Categorie' => 'required|int',
                'id_Type' =>  'required|int',
            ]);

            // si le formulaire est valide on continue le processus de création
            if ($form_validator->isValid()) {
                // insertion dans la base de données

                $money->hydrate($data);
                if (($money->id_Type == 2 ) && ($money->money_Moneycome < $money->getsum())) {
                    $money->setMoney_Moneycome(- $money->money_Moneycome);
                    
                
                    if ($money->update()) {
                        HttpRequest::redirectTo('index');
                    }
                }else if($money->id_Type == 1){
                    if ($money->update()) {
                        HttpRequest::redirectTo('index');
                    }
                }
                else{ echo "insertion impossible";}
               
            }
            // On renseigne le table d'erreurs passé à vue en cas d'erreurs.
            $errors = $form_validator->getErrors();
        }
        $type = new TypeModel();
        $types = $type->getAll();
        $categorie = new CategorieModel();
        $categories = $categorie->getAll();
        $cagnote = new CagnoteModel();
        $cagnotes = $cagnote->getAll();

        // compact : fonction php qui permet de créer un table assoc à partir de variables.
        echo $this->twig->render('moneycomes/update.html.twig', compact('title', 'types', 'categories', 'cagnotes', 'heading', 'data', 'errors'));
    }
    public function delete()
    {

        /**
         * 1. On récupère l'id du post
         */
        // on déclare la variable qui va récevoir l'id et on l'initialise avec rien.
        $money_id = Validator::checkId($_POST['id_Moneycome']);

        // s'il n'existe pas de paramètre, on affiche le message d'erreur

        if (!$money_id) {
            HttpRequest::redirectTo('index');
        }

        // Nouvel instance d'une somme

        $money = new MoneyModel();

        $money->setId_Moneycome($money_id);

        $money->delete("id_Moneycome");

        /**
         * On redirige vers le catalogue de cagnote
         */
        HttpRequest::redirectTo('index');
    }
}
