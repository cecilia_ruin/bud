<?php
namespace App\Controllers;
use App\Helpers\Validator;
use App\Helpers\HttpRequest;
use App\Models\MoneyCome as MoneyModel;
use App\Models\Cagnote as CagnoteModel;
use App\libraries\Form_Validator;

class Cagnote extends Controller
{
    /**
     * GET
     * Affiche la liste des cagnote (liste)
     * @return void
     */
    public function index()
    {
        // Création d'une nouvelle instance de Cagnote

        $cagnote = new CagnoteModel();
        /**
         * 2. On récupère la liste des cagnotes.
         */

        $cagnotes = $cagnote->getAll();
        /**
         * On affiche la page
         */
        $title = 'Accueil';

        $heading = 'Vos cagnotes';

        // compact : fonction php qui permet de créer un table assoc à partir de variables.
        echo $this->twig->render('cagnotes/index.html.twig', compact('cagnotes', 'heading', 'title'));
    }

    /**
     * GET
     * Affiche la page d'une cagnote (liste)
     * @return void
     */
    public function show()
    {
          /**
         * 1. On récupère le paramètre d'url id
         */
        // on déclare la variable qui va récevoir l'id et on l'initialise avec rien.
        $cagnote_id = Validator::checkId($_GET['id']);

        // s'il n'existe pas de paramètre, on affiche le message d'erreur

        if (!$cagnote_id) {
            HttpRequest::redirectTo('index');
        }

        // Nouvel instance de Cagnote

        $cagnote = new CagnoteModel();
        $cagnote->setId_Cagnote($cagnote_id);
        
        // récuparation d'une cagnote

        $cagnote->getOne('id_Cagnote');

        // récuparation de l'argent dessus

        $money=$cagnote->getAllMoneyComeByCagnote();
        $total=$cagnote-> getTotalMoneyComeByCagnote();

        /**
         * On affiche la page
         */ 
        $title = $cagnote->getName_Cagnote();

        $heading = $cagnote->getName_Cagnote();
        // compact : fonction php qui permet de créer un table assoc à partir de variables.
        echo $this->twig->render('cagnotes/show.html.twig', compact('title', 'heading', 'cagnote', 'money', 'total'));
    }
    /**
     * GET / POST
     * Affiche le formulaire de création 
     * Enregistre les données du formulaire dans la base de données
     * @return void
     */
    public function create()
    {
            // on initialise le tableau de données qui servira à pré-remplir les champs du formulaire
            $data = [
                'name_Cagnote' => '',
                'introduction_Cagnote' => '',
            ];
            // on initialise le tableau des erreurs à envoyer à la vue
            $errors = array();
            // On vérifie s'il y a une requête HTTP POST
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                // nettoyage des données utilisateurs
    
                // validation des données
    
                $data = $_POST;
                // die;
                // on crée une instance de la librairie de validation
                $form_validator = new Form_Validator();
    
                // on valide les données du formulaire avec les règles définies dans la table passé en second argument.
                $form_validator->make($data, [
                    'name_Cagnote' => 'required|alpha',
                    'introduction_Cagnote' => 'required|alpha',
                ]);
    
                // si le formulaire est valide on continue le processus de création
                if ($form_validator->isValid()) {
                    // insertion dans la base de données
                    // création d'une instance de cagnote
    
                    $cagnote = new CagnoteModel();
    
                    $cagnote->hydrate($data);
                    if ($cagnote->create()) {
                        HttpRequest::redirectTo('index');
                    }
                }
                // On renseigne le table d'erreurs passé à vue en cas d'erreurs.
                $errors = $form_validator->getErrors();
            }
            $title = 'Ajouter une cagnote';
            $heading = 'Enregister une nouvelle cagnote';
            echo $this->twig->render('cagnotes/create.html.twig', compact('title', 'heading','data', 'errors'));
    }
    /**
     * GET / POST
     * update une cagnote
     * @return void
     */
    public function update()
    {
        /**
         * 1. On récupère le paramètre d'url id
         */
        // on déclare la variable qui va récevoir l'id et on l'initialise avec rien.
        $cagnote_id = Validator::checkId($_GET['id']);

        // s'il n'existe pas de paramètre, on affiche le message d'erreur

        if (!$cagnote_id) {
            HttpRequest::redirectTo('index');
        }

        // Nouvel instance de Cagnote

        $cagnote = new CagnoteModel();

        $cagnote->setId_Cagnote($cagnote_id);

        // récuparation d'une cagnote

        $cagnote->getOne('id_Cagnote');

        /**
         * On affiche la page
         */
        $title = $cagnote->name_Cagnote;

        $heading = $cagnote->name_Cagnote;


        // partie création d'une Cagnote

        // on initialise le tableau de données qui servira à pré-remplir les champs du formulaire
        $data = [
            'id_Cagnote' => $cagnote->id_Cagnote,
            'name_Cagnote' => $cagnote->name_Cagnote,
            'introduction_Cagnote' => $cagnote->introduction_Cagnote,
        ];
        // on initialise le tableau des erreurs à envoyer à la vue
        $errors = array();
        // On vérifie s'il y a une requête HTTP POST
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // nettoyage des données utilisateurs

            // validation des données

            $data = $_POST;
            // die;
            // on crée une instance de la librairie de validation
            $form_validator = new Form_Validator();

            // on valide les données du formulaire avec les règles définies dans la table passé en second argument.
            $form_validator->make($data, [
                'name_Cagnote' => 'required|alpha',
                'introduction_Cagnote' => 'required|alpha',
            ]);

            // si le formulaire est valide on continue le processus de création
            if ($form_validator->isValid()) {
                // insertion dans la base de données

                $cagnote->hydrate($data);
                if ($cagnote->update()) {
                    HttpRequest::redirectTo('index');
                }
            }
            // On renseigne le table d'erreurs passé à vue en cas d'erreurs.
            $errors = $form_validator->getErrors();
        }

        // compact : fonction php qui permet de créer un table assoc à partir de variables.
        echo $this->twig->render('cagnotes/update.html.twig', compact('title', 'heading', 'data', 'errors'));
    }
    /**
     * GET / POST
     * Supprime une cagnote et ce qu'elle contient
     * @return void
     */
    public function delete()
    {

        /**
         * 1. On récupère l'id du post
         */
        // on déclare la variable qui va récevoir l'id et on l'initialise avec rien.
        $cagnote_id = Validator::checkId($_POST['id']);

        // s'il n'existe pas de paramètre, on affiche le message d'erreur

        if (!$cagnote_id) {
            HttpRequest::redirectTo('index');
        }

        // Nouvel instance de Cagnote

        $cagnote = new CagnoteModel();

        $cagnote->setId_Cagnote($cagnote_id);
        $cagnote->deletecontent();
        $cagnote->delete("id_Cagnote");
        /**
         * On redirige vers le catalogue des cagnotes
         */
        HttpRequest::redirectTo('index');
    }
}

