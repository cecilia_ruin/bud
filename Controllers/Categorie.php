<?php
namespace App\Controllers;
use App\Helpers\Validator;
use App\Helpers\HttpRequest;
use App\Models\Categorie as CatModel;
use App\Models\Cagnote as CagnoteModel;
use App\libraries\Form_Validator;

class Categorie extends Controller
{
    public function create()
    {
            // on initialise le tableau de données qui servira à pré-remplir les champs du formulaire
            $data = [
                'name_Categorie' => '',
            ];
            // on initialise le tableau des erreurs à envoyer à la vue
            $errors = array();
            // On vérifie s'il y a une requête HTTP POST
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                // nettoyage des données utilisateurs
    
                // validation des données
    
                $data = $_POST;
                // on crée une instance de la librairie de validation
                $form_validator = new Form_Validator();
    
                // on valide les données du formulaire avec les règles définies dans la table passé en second argument.
                $form_validator->make($data, [
                    'name_Categorie' => 'required|alpha',
                ]);
    
                // si le formulaire est valide on continue le processus de création
                if ($form_validator->isValid()) {
                    // insertion dans la base de données
                    // création d'une instance de Categorie
    
                    $categorie = new CatModel();
    
                    $categorie->hydrate($data);
                    if ($categorie->create()) {
                        HttpRequest::redirectTo('index');
                    }
                }
                // On renseigne le table d'erreurs passé à vue en cas d'erreurs.
                $errors = $form_validator->getErrors();
            }
            $title = 'Ajouter une cagnote';
            $heading = 'Enregister un nouveau livre';
            echo $this->twig->render('categories/create.html.twig', compact('title', 'heading','data', 'errors'));
    }
    /**
     * GET / POST
     * Modifie la catégorie
     * @return void
     */
    public function update()
    {
        /**
         * 1. On récupère le paramètre d'url id
         */
        // on déclare la variable qui va récevoir l'id et on l'initialise avec rien.
        $categorie_id = Validator::checkId($_GET['id']);

        // s'il n'existe pas de paramètre, on affiche le message d'erreur

        if (!$categorie_id) {
            HttpRequest::redirectTo('index');
        }

        // Nouvel instance de Book

        $categorie = new CatModel();

        $categorie->setId($categorie_id);

        // récuparation d'une cagnote

        $categorie->getOne('id_Categorie');

        /**
         * On affiche la page
         */
        $title = $categorie->name;

        $heading = $categorie->name;


        // partie création du livre

        // on initialise le tableau de données qui servira à pré-remplir les champs du formulaire
        $data = [
            'id_Categorie' => $categorie->id,
            'name_Categorie' => $categorie->name,
        ];
        // on initialise le tableau des erreurs à envoyer à la vue
        $errors = array();
        // On vérifie s'il y a une requête HTTP POST
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // nettoyage des données utilisateurs

            // validation des données

            $data = $_POST;
            // die;
            // on crée une instance de la librairie de validation
            $form_validator = new Form_Validator();

            // on valide les données du formulaire avec les règles définies dans la table passé en second argument.
            $form_validator->make($data, [
                'name_Categorie' => 'required|alpha',
            ]);

            // si le formulaire est valide on continue le processus de création
            if ($form_validator->isValid()) {
                // insertion dans la base de données
                // création d'une instance de book

                $categorie->hydrate($data);
                if ($categorie->update()) {
                    HttpRequest::redirectTo('index');
                }
            }
            // On renseigne le table d'erreurs passé à vue en cas d'erreurs.
            $errors = $form_validator->getErrors();
        }

        // compact : fonction php qui permet de créer un table assoc à partir de variables.
        echo $this->twig->render('cagnotes/update.html.twig', compact('title', 'heading', 'data', 'errors'));
    }
    /**
     * GET / POST
     * Affiche la page d'un livre (liste)
     * @return void
     */
    public function delete()
    {

        /**
         * 1. On récupère l'id du post
         */
        // on déclare la variable qui va récevoir l'id et on l'initialise avec rien.
        $categorie_id = Validator::checkId($_POST['id']);

        // s'il n'existe pas de paramètre, on affiche le message d'erreur

        if (!$categorie_id) {
            HttpRequest::redirectTo('index');
        }

        // Nouvel instance de Book

        $categorie = new CatModel();

        $categorie->setId($categorie_id);

        $categorie->delete();
        /**
         * On redirige vers le catalogue de livres
         */
        HttpRequest::redirectTo('index');
    }
}