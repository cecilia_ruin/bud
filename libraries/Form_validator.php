<?php

namespace App\Libraries;

use Error;

class Form_validator
{
    /**
     * Tableau d'erreurs
     *
     * @var array
     */
    private $errors = [];
    /**
     * Table des données à valider
     *
     * @var array
     */
    private $data = [];

    /**
     * Vérifie que les données sont bonnes
     *
     * @return boolean
     */
    public function isValid()
    {
        return count($this->errors) === 0;
    }
    /**
     * lance la validation des données
     *
     * @param array $data
     * @param array $rules
     * @return void
     */
    public function make(array $data, array $fields)
    {
        /*
         * On stocke le tableau de données dans une propriété de la classe pour pouvoir y accéder à l'intérieur des différentes méthodes.
         */
        $this->data = $data;
        foreach ($fields as $name => $rules) {
            // On vérifie que chaque clé du table tableau de validation est présent dans le tableau de données à valider.
            if (!array_key_exists($name, $data)) {
                throw new \Exception('Le champ ' . $name . ' est introuvable dans le tableau de données');
                return false;
            }
            /* On explose dans un tableau la chaine des règles de validation d'un champ au niveau du séparateur | */
            $rulesArray = explode('|', $rules);

            // on parcourt le tableau obtenu pour valider chaque règle.
            foreach ($rulesArray as $rule) {

                switch ($rule) {
                    case 'required':
                        $this->required($name);
                        break;
                    case 'email':
                        $this->email($name);
                        break;
                    case 'date':
                        $this->date($name);
                        break;
                    case 'alpha':
                        $this->alpha($name);
                        break;
                    case 'int':
                        $this->int($name);
                        break;
                    case 'url':
                        $this->url($name);
                        break;
                }
            }
        }
    }

    /**
     * Get the value of errors
     */
    public function getErrors()
    {
        return $this->errors;
    }

    public function required($field)
    {
        /*
         * on récupère la valeur du champ dans le tableau de données
         */
        $value = $this->data[$field];
        /*
         * Un considère comme vide la chaine vide, le null mais pas l'entier 0
         */
        if (empty(($value) || is_null($value)) && !is_numeric($value)) {
            $this->addError($field, 'Le champ ' . $field . ' est requis !');
        }
    }

    /**
     *Permet de valider des adresses de courrier électronique
     *
     * @param string $field
     * @return void
     */
    public function email($field)
    {

        /**
         * on récupère la valeur du champ dans le tableau de données
         */
        $value = $this->data[$field];
        /**
         * on valide le champ si elle n'est pas vide
         */
        if (!empty($field) && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $this->addError($field, 'L\'adresse mail renseignée n\'est pas valide !');
        }
    }

    /**
     * Permet de valider une adresse url
     *
     * @param string $field
     * @return void
     */
    public function url($field)
    {
        /*
         * on récupère la valeur du champ dans le tableau de données
         */
        $value = $this->data[$field];
        /*
         * on valide le champ si elle n'est pas vide
         */
        if (!empty($value) && !filter_var($value, FILTER_VALIDATE_URL)) {
            $this->addError($field, 'L\'url renseignée n\'est pas valide !');
        }
    }
    /*
     * Permet de valider des nombres entiers
     *
     * @param string $field
     * @return void
     */
    public function int($field)
    {
        $value = $this->data[$field];
        //var_dump( $this->data[$field]);

        /**
         * on valide le champ si elle est un nombre et il est de type entier
         */
        if (
            (isset($value) && $value !== '') && (filter_var($value, FILTER_VALIDATE_INT) === false)
        ) {
            $this->addError($field, 'Le nombre renseigné n\'est pas un entier valide !');
        }
    }
    /**
     * Permet de valider le format date Y-m-d
     *
     * @param string $field
     * @return void
     */
    public function date($field)
    {
        /**
         * on récupère la valeur du champ dans le tableau de données
         */
        $value = $this->data[$field];

        /**
         * on valide le champ si elle n'est pas vide
         */
        if (!empty($value) && !preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $value)) {
            $this->addError($field, 'La date renseignée n\'est pas un format de date valide !');
        }
    }
    /*
     * Permet de valider des chaines de caractères
     *
     * @param string $field
     * @return void
     */
    public function alpha($field)
    {
        /**
         * on récupère la valeur du champ dans le tableau de données
         */
        $value = $this->data[$field];
        /*
         * on valide le champ si elle n'est pas vide
         */
        if (!empty($value) && !preg_match_all("/^[A-ZÉéàèïëç0-9'\s-]+$/mi", $value)) {
            $this->addError($field, 'Le champ ' . $field . ' renseigné contient des caractères non autorisés !');
        }
    }
    /*
     * Permet d'enregistrer un message d'erreurs dans le tableau d'erreurs
     *
     * @param string $field
     * @param string $message
     * @return void
     */
    public function addError($field, $message)
    {
        $this->errors[$field] = $message;
    }
}
