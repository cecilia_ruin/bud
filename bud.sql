-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : mer. 26 jan. 2022 à 10:25
-- Version du serveur :  5.7.24
-- Version de PHP : 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bud`
--

-- --------------------------------------------------------

--
-- Structure de la table `cagnote`
--

CREATE TABLE `cagnote` (
  `id_Cagnote` int(11) NOT NULL,
  `name_Cagnote` varchar(150) DEFAULT NULL,
  `introduction_Cagnote` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cagnote`
--

INSERT INTO `cagnote` (`id_Cagnote`, `name_Cagnote`, `introduction_Cagnote`) VALUES
(1, 'Anniversaire Jo', 'Voici la cagnote pour l\'anniversaire de Jon '),
(4, 'Budget vacances', 'Calcul du budget disponible pour les vacances'),
(8, 'Cagnote 320', 'Voici la cagnote pour l\'anniversaire dejdkkjz');

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

CREATE TABLE `categorie` (
  `id_Categorie` int(11) NOT NULL,
  `name_Categorie` varchar(155) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id_Categorie`, `name_Categorie`) VALUES
(1, 'salaire'),
(2, 'don'),
(3, 'nouriture'),
(4, 'vetement'),
(5, 'loisir');

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE `clients` (
  `id_clients` int(11) NOT NULL,
  `name_clients` varchar(150) DEFAULT NULL,
  `mail_clients` varchar(255) NOT NULL,
  `password_client` varchar(255) NOT NULL,
  `id_cagnote` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `moneycome`
--

CREATE TABLE `moneycome` (
  `id_Moneycome` int(11) NOT NULL,
  `name_Moneycome` varchar(155) DEFAULT NULL,
  `money_Moneycome` float DEFAULT NULL,
  `id_Cagnote` int(11) DEFAULT NULL,
  `id_Categorie` int(11) DEFAULT NULL,
  `id_Type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `moneycome`
--

INSERT INTO `moneycome` (`id_Moneycome`, `name_Moneycome`, `money_Moneycome`, `id_Cagnote`, `id_Categorie`, `id_Type`) VALUES
(1, 'Participation Robert', 25, 4, 3, 2),
(2, 'Participation Lea', 30, 1, 2, 1),
(13, 'test', 58, 1, 2, 1),
(15, 'Participation Dan', 20, 1, 2, 1),
(16, 'Participation Leo', 27, 1, 2, 1),
(17, 'participation bidule', 20, 1, 2, 1),
(19, 'Participation Rob', 35, 4, 3, 1),
(20, 'Participation Dan', -20, 1, 3, 2);

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

CREATE TABLE `type` (
  `id_Type` int(11) NOT NULL,
  `name_Type` varchar(155) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type`
--

INSERT INTO `type` (`id_Type`, `name_Type`) VALUES
(1, 'income'),
(2, 'outcome');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `cagnote`
--
ALTER TABLE `cagnote`
  ADD PRIMARY KEY (`id_Cagnote`);

--
-- Index pour la table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id_Categorie`);

--
-- Index pour la table `moneycome`
--
ALTER TABLE `moneycome`
  ADD PRIMARY KEY (`id_Moneycome`),
  ADD KEY `FK_MoneyCome_id_Cagnote` (`id_Cagnote`),
  ADD KEY `FK_MoneyCome_id_Categorie` (`id_Categorie`),
  ADD KEY `FK_MoneyCome_id_Type` (`id_Type`);

--
-- Index pour la table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id_Type`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `cagnote`
--
ALTER TABLE `cagnote`
  MODIFY `id_Cagnote` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id_Categorie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `moneycome`
--
ALTER TABLE `moneycome`
  MODIFY `id_Moneycome` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `type`
--
ALTER TABLE `type`
  MODIFY `id_Type` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `moneycome`
--
ALTER TABLE `moneycome`
  ADD CONSTRAINT `FK_MoneyCome_id_Cagnote` FOREIGN KEY (`id_Cagnote`) REFERENCES `cagnote` (`id_Cagnote`),
  ADD CONSTRAINT `FK_MoneyCome_id_Categorie` FOREIGN KEY (`id_Categorie`) REFERENCES `categorie` (`id_Categorie`),
  ADD CONSTRAINT `FK_MoneyCome_id_Type` FOREIGN KEY (`id_Type`) REFERENCES `type` (`id_Type`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
